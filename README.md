# TASK MANAGER

# DEVELOPER INFO

NAME: Nadezhda Irinarkhova

E-mail: nirinarkhova@tsconsulting.com

E-mail: nirinarkhova@gmail.com

# SOFTWARE

* JDK 1.8

* WINDOWS 10

# HARDWARE

* RAM 16

* CPU I7

* HDD 256

# BUILD PROGRAM

```
mvn clean install
```

# RUN PROGRAMM

```bash
java -jar ./task-manager.jar
```

# SCREENSOTS

https://drive.google.com/drive/folders/1r2W-HQIydDcADohNEGoP9UMzgcOFt76_?usp=sharing
